import './App.css';
import FormOtp from "./components/otpForm";
import {BrowserRouter as Router} from "react-router-dom";

function App() {
    return (
        <Router>
            <FormOtp></FormOtp>
        </Router>
    );
}

export default App;
