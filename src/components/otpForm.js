
import React, { useState } from "react";
import { Route, Link, Routes, useNavigate } from "react-router-dom";


function FormOtp() {

    // This does GET method
    function promiseGetMethod() {

        const url = 'http://82.115.17.246:8000/api/auth/type';

         fetch(url, {method: 'GET'}).then((response) => {
            if (response.ok) {
                return response.json();
            }
            throw new Error('Something went wrong');
        })
            .then((responseJson) => {

                console.log(responseJson);

            }).catch((error) => {
            console.log(error + ", or Please check your internet connection and try again.");
        })
    }

    //This does POST method
    const [data, setData] = useState({
        mobile: "",
        password: ""
    })

    function handle(e) {

        const newData = { ...data };
        newData[e.target.id] = e.target.value;
        setData(newData);
    }

    let navigate = useNavigate();

    function submit(e) {

        e.preventDefault();

        //do GET action
        promiseGetMethod();

        let url = "http://82.115.17.246:8000/api/auth";

        let myHeaders = new Headers();
        myHeaders.append("Accept", "application/json");

        let urlencoded = new URLSearchParams();
        urlencoded.append("mobile", data.mobile);
        urlencoded.append("password", data.password);

        let requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: urlencoded,
        };

        //These variables are getting Object's
        let objContainResponse;
        let getStatus;
        let getBody;

        fetch(url, requestOptions).then((response) => {

            getStatus = response.status;
            getBody = response.body;
            objContainResponse = {
                "status": getStatus,
                "body": getBody
            };
            console.log(objContainResponse)

            if(response.ok) {

                navigate("/dashboard");
            }else {
                navigate("/failed");
            }
        })
    }

    const successfulLogin = () => (

        <div style={{textAlign: "center"}}>
            <p>Welcome, You successfully logged in :)</p>
        </div>
    )
    const failedLogin = () => (

        <div style={{textAlign: "center"}}>
            <p>Incorrect username or password, Try again :(</p>
        </div>
    )
    const cities = () => (

        <div>
            <ul>
                <li>NYC</li>
                <li>CAL</li>
                <li>IST</li>
            </ul>
        </div>
    )
    const home = () => (

        <div>
            <h2>Home</h2>
        </div>
    )
    const notFound = () => (

        <div>
            <h2>404 NotFound</h2>
        </div>
    )

    return (
        <div>
            <form style={{textAlign: "center", paddingTop: "50px"}} onSubmit={(e)=> submit(e)}>
                <label>
                    <b>mobile:</b>
                </label>
                <br/>
                <br/>
                <input placeholder={"Mobile Phone"} type={"number"} onChange={(e)=>handle(e)} id={'mobile'} value={data.mobile}/>
                <br/>
                <br/>
                <br/>
                <label>
                    <b>Password:</b>
                </label>
                <br/>
                <br/>
                <input placeholder={"Password"} type={"text"} onChange={(e)=>handle(e)} id={'password'} value={data.password}/>
                <br/>
                <br/>
                <button>Submit!</button>
            </form>
            <div>
                <ul>
                    <li>
                        <Link to={'/'}>Home</Link>
                    </li>
                    <li>
                        <Link to={'/cities'}>Cities</Link>
                    </li>
                    <Routes>
                        <Route path={"/"} element={home()}/>
                        <Route path={"/cities"} element={cities()}/>
                        <Route path={"/dashboard"} element={successfulLogin()}/>
                        <Route path={"/failed"} element={failedLogin()}/>
                        <Route path="*" element={notFound()}/>
                    </Routes>
                </ul>
            </div>
        </div>
    );
}

export default FormOtp;


